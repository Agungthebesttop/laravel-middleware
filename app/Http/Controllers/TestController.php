<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TestController extends Controller
{
    public function test1() {
        return 'Berhasil masuk route-1';
    }

    public function test2() {
        return 'Berhasil masuk route-2';
    }

    public function test3() {
        return 'Berhasil masuk route-3';
    }
}
